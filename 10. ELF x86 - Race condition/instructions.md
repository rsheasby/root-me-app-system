# ELF x86 - Race condition

This problem is very simple. Basically the program will write the password into a temporary file at `/tmp/tmp_file.txt` and then delete it 1/4 of a second later. All we need to do is start the program and then instantly read the file. We can do that like so: `./ch12 & cat /tmp/tmp_file.txt`.
