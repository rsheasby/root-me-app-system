# 2. Bash - System 2

As you can see from the source code, the program is using the command line to run commands. The "ls" command is not using an absolute path meaning we can make it run a different command by modifying the path like so, although in this case we need to create a script to run cat and ignore the flags. Create a file to do so called /tmp/ls and then invoke ch12 with a modified path:

```
$ PATH=/tmp:$PATH ./ch12 
8a95eDS/*e_T#
```
